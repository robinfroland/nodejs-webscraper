const cheerio = require('cheerio');
const axios = require('axios');
const fs = require('fs');

const baseURL = 'https://no.wikipedia.org/'
// Norwegian words and expressions
const categoryURL = '/wiki/Kategori:Norske_ord_og_uttrykk';

axios.get(baseURL + categoryURL).then(response => {
    const $ = cheerio.load(response.data);
    
    let json = {
        words_and_expressions: []
    };

    // For each li within .mw-category class
    $('.mw-category li').each((i, elem) => {
        // word or expression
        const title = $(elem).text();
        // Reference to read more about word/expression
        const refURL = baseURL + $(elem).find('a').attr('href');

        json.words_and_expressions.push({
            title: title,
            reference: `Read more: ${refURL}`
        });  
    });

    fs.writeFile('words-expressions.json', JSON.stringify(json, null, 2),  (error) => {
        if (error) {
            console.log(error.message);
            throw error;
        }
        console.log("Scrape and write successful");
    });
    
});
